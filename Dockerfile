FROM alpine:3.12
LABEL Name=dockersslgenerator Version=0.0.1 Maintainer="Siriwat Jiwsuwan <siriwat.jiwsuwan@gmail.com>"

ENV CA_KEY=ca.key
ENV CA_CERT=ca-cert.pem
ENV CA_SUBJECT=docker-ca
ENV CA_EXPIRE=3650
ENV SSL_CONFIG=openssl.cfg
ENV SSL_KEY=docker.key
ENV SSL_CSR=docker.csr
ENV SSL_CERT=docker-cert.pem
ENV SSL_SIZE=4096
ENV SSL_EXPIRE=3650
ENV SSL_MSG_DIG=sha512
ENV SSL_SUBJECT=docker
ENV SSL_DNS=example.com
ENV SSL_IP=127.0.0.1
ENV DEBUG=1

RUN apk --update --no-cache add openssl \
    rm -rf /var/lib/apt/lists/* && \
    rm -f /var/cache/apk/*
    
WORKDIR /certs

COPY generate-certs /usr/local/bin/generate-certs

ENTRYPOINT ["/usr/local/bin/generate-certs"]
CMD ["--help"]

VOLUME /certs
